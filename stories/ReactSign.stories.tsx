import { Meta, Story } from '@storybook/react';
import React from 'react';
import ReactSign from '../src';

const meta: Meta = {
  title: 'React Sign',
  component: ReactSign,
  argTypes: {
    children: {
      control: {
        type: 'text',
      },
    },
  },
  parameters: {
    controls: { expanded: true },
  },
};

export default meta;

const ReactSign2: Story = (args) => {
  const onEnter = (count: number) => {
    console.log(`Component ${count} is in port`);
  };
  const onExit = (count: number) => {
    console.log(`Component ${count} is't in view port`);
  };
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <h3>scroll down and check the console logs... 👇👇</h3>
      <div style={{ height: '2500px' }} />
      <ReactSign onEnter={() => onEnter(1)} onLeave={() => onExit(1)}>
        <section
          style={{
            height: '250px',
            border: '1px dashed red',
            alignItems: 'center',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
          }}
        >
          <h3>Component 1</h3>
          <p>Check logs</p>
        </section>
      </ReactSign>
      <ReactSign onEnter={() => onEnter(2)} onLeave={() => onExit(2)}>
        <section
          style={{
            height: '250px',
            border: '1px dashed red',
            alignItems: 'center',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
          }}
        >
          <h3>Component 2</h3>
          <p>Check logs</p>
        </section>
      </ReactSign>
    </div>
  );
};

// By passing using the Args format for exported stories, you can control the props for a component for reuse in a test
// https://storybook.js.org/docs/react/workflows/unit-testing
export const Default = ReactSign2.bind({});

Default.args = {};
