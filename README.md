# React Sign

Pass onEnter or onLeave functions to detect when component shows on user's screen to load more data in lists or play animations.

## Instalation

##### Using npm:

`$ npm install react-sign`

##### Using yarn:

`$ yarn add react-sign`

## Usage

Put React sign where you want detect user scrolled there.

```javascript
<ReactSign onEnter={() => doSomthing()} onLeave={() => doAnotherThing()} />
```

Or Put your component (block of code) in React sign.

```javascript
const ReactSign2: Story = (args) => {
  const onEnter = () => {
    console.log('Component is in');
  };
  const onExit = () => {
    console.log("Component isn't in");
  };
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <h3>scroll down 👇👇</h3>
      <div style={{ height: '2500px' }} />

      <ReactSign onEnter={onEnter} onLeave={onExit}>
        <div
          style={{
            height: '250px',
            border: '1px dashed red',
            alignItems: 'center',
            display: 'flex',
            justifyContent: 'center',
          }}
        >
          <p>I am here...</p>
        </div>
      </ReactSign>
    </div>
  );
};
```

Also you can set a threshold to run onEnter and onExit after that.
Threshold should be between 0 and 1.

For example to run functions in %50 of component height you can do like this:

```javascript
<ReactSign threshold={0.5} />
```
